The combustor to post combustor piping subassembly connects the combustor to the post combustor plenum. It consists of two pipe fittings: a flange and the entrance to the post combustor plenum. 

This sub-assembly is self-contained and does not require specifying any physical parameters.